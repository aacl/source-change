package com.gitlab.aacl.sourcechange.analysis;

import com.gitlab.aacl.sourcechange.core.RepositoryLoader;
import com.gitlab.aacl.sourcechange.core.SourceFile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SourceAnalyzer {
    private static final Logger LOGGER = LoggerFactory.getLogger(SourceAnalyzer.class);
    private RepositoryLoader repoLoader;

    public SourceAnalyzer(RepositoryLoader repositoryLoader) {
        this.repoLoader = repositoryLoader;
    }

    public Object run() {
        try {
            repoLoader
                    .getCurrentFiles()
                    .forEach(this::logFile)
                    .get();
        } catch (Exception ex) {
            LOGGER.error("", ex);
        }
        return new Object();
    }

    private void logFile(SourceFile file) {
        String msg = String.format("File %s at %s", file.getName(), file.getPath());
        LOGGER.info(msg);
    }
}
