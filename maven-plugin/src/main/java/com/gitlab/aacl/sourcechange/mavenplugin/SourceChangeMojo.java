package com.gitlab.aacl.sourcechange.mavenplugin;

import com.gitlab.aacl.sourcechange.core.RepositoryLoader;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;

import javax.inject.Inject;

@Mojo(name = "source-change", defaultPhase = LifecyclePhase.SITE)
public class SourceChangeMojo extends AbstractMojo {

    @Parameter(defaultValue = "git", required = true, readonly = true)
    private String versionControlSystem;

    public void setVersionControlSystem(String value) {
        this.versionControlSystem = value;
    }

    private RepositoryLoader loader;
    @Inject
    public void setRepositoryLoader(RepositoryLoader loader) {
        this.loader = loader;
    }

    @Override
    public void execute() throws MojoExecutionException, MojoFailureException {

    }
}