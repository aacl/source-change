package com.gitlab.aacl.sourcechange.core;

import com.gitlab.aacl.sourcechange.core.async.AsyncIterator;
import com.gitlab.aacl.sourcechange.core.async.Generator;

public interface RepositoryLoader {
    /***
     * Gets the current tracked files
     * @return
     */
    AsyncIterator<SourceFile> getCurrentFiles();
}
