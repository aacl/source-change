package com.gitlab.aacl.sourcechange.core;

import java.nio.file.Path;
import java.util.concurrent.atomic.AtomicReference;

public class Config {
    private final AtomicReference<Path> localCacheLocation;

    public Config() {
        localCacheLocation = new AtomicReference<>();
    }

    public Config setLocalCacheLocation(Path path) {
        this.localCacheLocation.set(path);
        return this;
    }

    public Path getLocalCachePath() {
        return this.localCacheLocation.get();
    }
}
