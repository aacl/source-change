package com.gitlab.aacl.sourcechange.core.async;

import java.util.concurrent.CompletableFuture;

public interface Generator<T> extends AsyncIterator<T> {
    boolean tryAdd(T item);
    void addSource(CompletableFuture<Void> future);
}
