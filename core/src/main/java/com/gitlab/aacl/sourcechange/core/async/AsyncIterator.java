package com.gitlab.aacl.sourcechange.core.async;

import java.util.concurrent.CompletableFuture;
import java.util.function.Consumer;
import java.util.function.Function;

public interface AsyncIterator<T> extends AutoCloseable {
    CompletableFuture<Void> forEach(Consumer<T> consumer);
    <Out> AsyncIterator<Out> map(Function<T, Out> map);
    AsyncIterator<T> filter(Function<T, Boolean> filter);
}