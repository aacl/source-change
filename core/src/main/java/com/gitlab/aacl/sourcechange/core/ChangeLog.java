package com.gitlab.aacl.sourcechange.core;

public class ChangeLog {
    private final SourceFile file;

    public ChangeLog(SourceFile file) {
        this.file = file;
    }
}
