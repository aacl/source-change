package com.gitlab.aacl.sourcechange.core;

public class SourceFile {
    private final String path;
    private final String name;
    private final Object hash;

    private SourceFile(String path, String name, Object hash) {
        this.path = path;
        this.name = name;
        this.hash = hash;
    }

    public String getPath() {
        return path;
    }

    public String getName() {
        return name;
    }

    public Object getHash() {
        return hash;
    }

    public static class Builder {
        private String path;
        private String name;
        private Object hash;

        public Builder path(String path) {
            this.path = path;
            return this;
        }

        public Builder name(String name) {
            this.name = name;
            return this;
        }

        public Builder hash(Object hash) {
            this.hash = hash;
            return this;
        }

        public SourceFile build() {
            return new SourceFile(
                    path,
                    name,
                    hash
            );
        }
    }
}
