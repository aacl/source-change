package com.gitlab.aacl.sourcechange.core.async;

import reactor.core.Disposable;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Sinks;
import reactor.core.scheduler.Schedulers;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Future;
import java.util.function.Consumer;
import java.util.function.Function;

/***
 * TODO: Need to research more about this implementation
 * https://stackoverflow.com/questions/58504527/is-mono-tofuture-blocking
 * Not sure if Flux is the way to go, and hiding it behind CompleteableFuture
 * seems to be a decent compromise even if not recommended.
 *
 * The interface is still very much influx and unstable.
 * @param <T>
 */
public class GeneratorReactive<T> implements Generator<T> {
    private final Flux<T> flux;
    private final Sinks.Many<T> sink;
    private Optional<Disposable> subscription;
    private final List<CompletableFuture<Void>> waitableSources;

    public GeneratorReactive() {
        sink = Sinks
                .many()
                .multicast()
                .onBackpressureBuffer();
        flux = sink
                .asFlux()
                .subscribeOn(Schedulers.boundedElastic());
        subscription = Optional.empty();
        waitableSources = new ArrayList<>();
    }

    @Override
    public CompletableFuture<Void> forEach(Consumer<T> next) {
        CompletableFuture<Void> future = new CompletableFuture<>();
        Disposable subscribe = flux.subscribe(
                next,
                future::completeExceptionally,
                () -> {
                    future.complete(null);
                }
        );
        subscription = Optional.of(subscribe);
        return future;
    }

    @Override
    public <Out> AsyncIterator<Out> map(Function<T, Out> map) {
        return TransFormIterator.fromMap(flux, map);
    }

    @Override
    public AsyncIterator<T> filter(Function<T, Boolean> filter) {
        return TransFormIterator.fromFilter(flux, filter);
    }

    @Override
    public boolean tryAdd(T item) {
        return sink.tryEmitNext(item).isSuccess();
    }

    @Override
    public void addSource(CompletableFuture<Void> future) {
        waitableSources.add(future);
    }

    @Override
    public void close() throws Exception {
        try {
            getAllPendingFutures().get();
        } catch (Exception ex) {
            sink.emitError(ex, (x, y) -> true);
        } finally {
            subscription.ifPresent(Disposable::dispose);
            int count = 0;
            while (sink.tryEmitComplete().isFailure() && count <1000) {
                count++;
            }
        }
    }

    private CompletableFuture<Void>getAllPendingFutures() {
        int size = waitableSources.size();
        CompletableFuture[] futures = new CompletableFuture[size];
        waitableSources.toArray(futures);
        // CompletableFuture<Void>[] futures = waitableSources.stream().toArray(CompletableFuture[]::new);
        return CompletableFuture.allOf(futures);
    }

    public static class TransFormIterator<T> implements AsyncIterator<T> {
        private final Flux<T> flux;
        private Optional<Disposable> subscription;

        public TransFormIterator(Flux<T> flux) {
            this.flux = flux;
        }

        @Override
        public CompletableFuture<Void> forEach(Consumer<T> next) {
            CompletableFuture<Void> future = new CompletableFuture<>();
            Disposable subscribe = flux.subscribe(
                    next,
                    future::completeExceptionally,
                    () -> {
                        future.complete(null);
                    }
            );
            subscription = Optional.of(subscribe);
            return future;
        }

        @Override
        public <Out> AsyncIterator<Out> map(Function<T, Out> map) {
            return fromMap(flux, map);
        }

        @Override
        public AsyncIterator<T> filter(Function<T, Boolean> filter) {
            return fromFilter(flux, filter);
        }

        @Override
        public void close() throws Exception {
            subscription.ifPresent(Disposable::dispose);
        }

        public static <T,O> AsyncIterator<O> fromMap(
                Flux<T> flux,
                Function<T, O> map
        ) {

            return new TransFormIterator<>(flux.map(map));
        }

        public static <T> AsyncIterator<T> fromFilter(
                Flux<T> flux,
                Function<T, Boolean> filter
        ) {
            return new TransFormIterator<>(flux.filter(filter::apply));
        }
    }
}
