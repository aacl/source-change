package com.gitlab.aacl.sourcechange.git;

import javax.inject.Singleton;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;

@Singleton
public class GitRepoConfig {

    private final AtomicBoolean local;
    private final AtomicReference<URL> remoteUrl;
    private final AtomicReference<Path> localPath;

    public GitRepoConfig() {
        this.local = new AtomicBoolean(false);
        this.remoteUrl = new AtomicReference<>();
        this.localPath = new AtomicReference<>();
    }

    public boolean isLocal() {
        return this.local.get();
    }

    public void setIsLocal(boolean next) {
        boolean current = local.get();
        spinAndSet(local, current, next);
    }

    public void setRemoteUrl(String url)
            throws MalformedURLException {
        URL current = remoteUrl.get();
        URL asUrl = new URL(url);
        spinAndSet(remoteUrl, current, asUrl);
    }

    public URL getRemoteUrl() {
        return remoteUrl.get();
    }

    public void setRepoLocalPath(String next) {
        Path current = localPath.get();
        Path path = Paths.get(next);
        spinAndSet(this.localPath, current, path);
    }

    public Path getRepoLocalPath() {
        return this.localPath.get();
    }

    private static <T> void spinAndSet(
            AtomicReference<T> ref,
            T current,
            T newValue
    ) {
        while (!ref.compareAndSet(current, newValue)) {
            current = ref.get();
        }
    }

    private static void spinAndSet(
            AtomicBoolean ref,
            boolean current,
            boolean newValue
    ) {
        while (!ref.compareAndSet(current, newValue)) {
            current = ref.get();
        }
    }
}
