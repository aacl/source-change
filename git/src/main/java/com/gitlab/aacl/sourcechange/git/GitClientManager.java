package com.gitlab.aacl.sourcechange.git;

import org.eclipse.jgit.api.Git;

import java.io.IOException;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;

public interface GitClientManager {
    Git getClient() throws IOException;

    CompletableFuture<Optional<Git>> synchronizeRepo();
}
