package com.gitlab.aacl.sourcechange.git;

import com.gitlab.aacl.sourcechange.core.Config;
import org.eclipse.jgit.api.CloneCommand;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Named;
import javax.inject.Singleton;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Path;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;

@Singleton
@Named(AvailableServices.DefaultGitClientManager)
public class DefaultGitClientManager implements GitClientManager {
    private static final Logger LOGGER = LoggerFactory.getLogger(GitRepositoryLoader.class);

    private final Config sourChangeConfig;
    private final GitRepoConfig gitRepoConfig;
    public DefaultGitClientManager(
            Config sourChangeConfig,
            GitRepoConfig gitRepoConfig
    ) {
        this.sourChangeConfig = sourChangeConfig;
        this.gitRepoConfig = gitRepoConfig;
    }

    @Override
    public Git getClient() throws IOException {
        return Git.open(gitRepoConfig.getRepoLocalPath().toFile());
    }

    @Override
    public CompletableFuture<Optional<Git>> synchronizeRepo() {
        if (isRepoSpecified()) {
            if (!gitRepoConfig.isLocal()) {
                return CompletableFuture.supplyAsync(this::runCloneRepo);
            }
        }
        return CompletableFuture.completedFuture(Optional.empty());
    }

    private Optional<Git> runCloneRepo() {
        URL url = gitRepoConfig.getRemoteUrl();
        Path localCachePath = sourChangeConfig.getLocalCachePath();
        try (
                Git gitClient = Git
                        .cloneRepository()
                        .setURI(url.toString())
                        .setDirectory(localCachePath.toFile())
                        .call()
        ) {
            String file = localCachePath.toString() + url.getPath().replace(".git", "");
            gitRepoConfig.setRepoLocalPath(file);
            gitRepoConfig.setIsLocal(true);
            return Optional.of(gitClient);
        } catch (Exception ex) {
            LOGGER.error("Couldn't clone repo", ex);
            return Optional.empty();
        }
    }

    public boolean isRepoSpecified() {
        return gitRepoConfig.isLocal() || gitRepoConfig.getRemoteUrl() != null;
    }
}
