package com.gitlab.aacl.sourcechange.git;

public final class AvailableServices {
    public static final String GitRepoLoader = "GitRepoLoader";
    public static final String DefaultGitClientManager = "DefaultGitClientManager";
}
