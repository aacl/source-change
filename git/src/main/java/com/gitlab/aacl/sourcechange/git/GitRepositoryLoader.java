package com.gitlab.aacl.sourcechange.git;

import com.gitlab.aacl.sourcechange.core.RepositoryLoader;
import com.gitlab.aacl.sourcechange.core.SourceFile;
import com.gitlab.aacl.sourcechange.core.async.AsyncIterator;
import com.gitlab.aacl.sourcechange.core.async.Generator;

import com.gitlab.aacl.sourcechange.core.async.GeneratorReactive;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.lib.*;
import org.eclipse.jgit.revwalk.RevCommit;
import org.eclipse.jgit.revwalk.RevTree;
import org.eclipse.jgit.revwalk.RevWalk;
import org.eclipse.jgit.treewalk.TreeWalk;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.concurrent.*;

@Named(AvailableServices.GitRepoLoader)
public class GitRepositoryLoader implements RepositoryLoader, AutoCloseable {
    private static final Logger LOGGER = LoggerFactory.getLogger(GitRepositoryLoader.class);
    private GitClientManager gitClientManager;
    private final ExecutorService executorService;


    @Inject // Executors.newCachedThreadPool()
    public GitRepositoryLoader(
            GitClientManager gitClientManager,
            ExecutorService executorService
    ) {
        this.gitClientManager = gitClientManager;
        this.executorService = executorService;
    }

    public AsyncIterator<SourceFile> getCurrentFiles() {
        Generator<SourceFile> generator = new GeneratorReactive<>();
        Runnable r = () -> {
            try (
                    Generator<SourceFile> gen = generator;
                    Git gitClient = gitClientManager.getClient();
                    Repository repo = gitClient.getRepository()
            ) {

                ObjectId lastCommitId = repo.resolve(Constants.HEAD);
                try (RevWalk revWalk = new RevWalk(repo)) {
                    RevCommit commit = revWalk.parseCommit(lastCommitId);
                    RevTree tree = commit.getTree();
                    try (TreeWalk treeWalk = new TreeWalk(repo)) {
                        treeWalk.addTree(tree);
                        treeWalk.setRecursive(true);
                        int count = 0;
                        SourceFile.Builder builder = new SourceFile.Builder();
                        while (treeWalk.next()) {
                            ObjectId obj = treeWalk.getObjectId(count);
                            ObjectLoader loader = repo.open(obj);
                            String path = treeWalk.getPathString();
                            String name = obj.getName();
                            int hash = obj.hashCode();
                            long size = loader.getSize();
                            builder
                                    .name(name)
                                    .hash(hash)
                                    .path(path);

                            gen.tryAdd(builder.build());
                        }
                    }
                }
            } catch (Exception ex) {
                LOGGER.error("", ex);
            }
        };

        CompletableFuture<Void> pendingTask = CompletableFuture.runAsync(r, executorService);
        generator.addSource(pendingTask);
        return generator;
    }

    public void close() throws Exception {
        executorService.shutdown();
        // temp setting for now
        executorService.awaitTermination(1, TimeUnit.MINUTES);
    }
}
